const host = "http://47.104.145.74:8888"

const wxRequest = (params, url) => {
  wx.showToast({
    title: '加载中',
    icon: 'loading'
  })
  console.log(params);
  console.log(url); 
  wx.request({
    url: url,
    method: params.method || 'POST',
    data: params.data || {},
    header: {
      'content-type': 'application/json'
    },
    success: (res) => {
      params.success && params.success(res)
      wx.hideToast()
    },
    fail: (res) => {
      params.fail && params.fail(res)
      console.log(res)
    },
    complete: (res) => {
      params.complete && params.complete(res)
    }
  })
}

// Index
// const getTicketList = (params) => wxRequest(params, host + 'smallpro/ticket/scenicList.htm');
const signIn = (params) => wxRequest(params, host + '/apincar/wx/user/login');
const publishTrip = (params) => wxRequest(params, host + '/apincar/trip/publishTrip');

module.exports = {
  // getTicketList,
  signIn,
  publishTrip
}
