// pages/brush/brush.js
const app = getApp();
const Api = require('../../api/api.js');
const Utils = require('../../utils/util.js');
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 1: 人找车，2: 车找人
    currentType: null,
    endPoint: {
      title:'',
      location:{}
    },
    startPoint: {
      title: '',
      location: {}
    },
    startTime:'',
    renderDate:'',
    // 时间选择器
    minHour: 10,
    maxHour: 20,
    minDate: new Date().getTime(),
    currentDate: new Date().getTime(),
    timeBoxShow:false,
    // 备注
    remark:'',
    // 人数
    seatNum:null
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    console.log(options)
    //初始化表单验证
    this.WxValidate = app.WxValidate (
      {
        startPoint: {
          required: true
        },
        endPoint: {
          required: true
        },
        seatNum: {
          required: true
        }
      }, 
      {
        startPoint: {
          required: '请填写出发地',
        },
        endPoint: {
          required: '请填写目的地',
        },
        seatNum: {
          required: options.index === 1 ? '请填写乘坐人数' : '请填写可载人数',
        }
      }
    )

    this.setData({
      currentType: options.type
    })
  },
  startPoint(){
    app.getLocation('startPoint');
  },
  endPoint(){
    app.getLocation('endPoint');
  },
  // 时间选择器
  confirmTimeBox(event){
    console.log(event)
    this.setData({
      currentDate: event.detail,
      timeBoxShow: false,
      startTime: Utils.formatTime(new Date(event.detail))
    });
  },
  // cancle
  cancleTimeBox(event){
    this.setData({
      timeBoxShow:false
    })
  },
  // 选择开始时间
  selectStartTime(){
    this.setData({
      timeBoxShow: true
    })
  },
  changeSeatNum({ detail }){
    this.setData({
      seatNum:detail
    })
  },
  //表单验证
  formSubmit: function (e) {
    const that = this;
    //提交错误描述
    if (!this.WxValidate.checkForm(e)) {
      const error = this.WxValidate.errorList[0]
      // `${error.param} : ${error.msg} `
      wx.showModal({
        showCancel: false,
        title: '提交错误',
        content: `${error.msg} `
      })
      return false
    }
    
   let submitObj = {
     "endName": that.data.endPoint.title,
     "endPoint": that.data.endPoint.location,
     "id": 0,
     "remark": that.data.startTime,
     "seatNum": that.data.seatNum,
     "startName": that.data.startPoint.title,
     "startOffTime": that.data.startTime,
     "startPoint": that.data.startPoint.location,
     "type": that.data.currentType
   }
    console.log(submitObj)

    Api.publishTrip({
      data: submitObj,
      success: (res) => {
        console.log(res)
        if (!res.code) {
          wx.showToast({
            title: res.data.message,
            icon: 'success',
            duration: 2000
          })
        }
        // pagethis.setData({
        //   response: res.data
        // })
      },
      fail: (res) => {
        console.log('++++++++++++错误信息++++++++++++++')
        console.log(res)
      }
    }) 
  },
})