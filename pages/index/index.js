//index.js
//获取应用实例
const app = getApp();
const Api = require('../../api/api.js');

Page({
  data: {
    motto: 'Hello World',
    userInfo: {},
    hasUserInfo: false,
    canIUse: wx.canIUse('button.open-type.getUserInfo'),
    // Banner
    banner: {
      imgUrls: [
        'https://statics.lotsmall.cn/image/20181227/20181227222505nj1y20.jpeg?imageMogr2/thumbnail/640x/strip/quality/50',
        'https://statics.lotsmall.cn/image/20181106/20181106192255y23y5i.jpg?imageMogr2/thumbnail/640x/strip/quality/50',
        'https://statics.lotsmall.cn/image/20181106/20181106190437uhfsy0.jpg?imageMogr2/thumbnail/640x/strip/quality/50'
      ],
      indicatorDots: true,
      indicatorColor: 'rgba(255,255,255,0.6)',
      indicatorActiveColor: '#fff',
      autoplay: true,
      interval: 5000,
      duration: 1000,
      circular: true
    },
    // touchbar
    current: 'homepage',
    // 发布类型选择
    brushTypeShow: false,
    actions: [
      {
        name: '我要乘车',
        color: '#2d8cf0'
      },
      {
        name: '我要载客',
        color: '#19be6b'
      }
    ],
    // 地址信息
    startPlace: {
      title: '北京',
      location: {

      }
    },
    endPlace: {
      title: '',
      location: {

      }
    }
  },
  //事件处理函数
  bindViewTap: function () {
    wx.navigateTo({
      url: '../logs/logs'
    })
  },
  onLoad: function () {
    if (app.globalData.userInfo) {
      console.log(app.globalData.userInfo)
      this.signIn();
      this.setData({
        userInfo: app.globalData.userInfo,
        hasUserInfo: true
      })
    } else if (this.data.canIUse) {
      // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
      // 所以此处加入 callback 以防止这种情况
      app.userInfoReadyCallback = res => {
        Object.assign(app.globalData, res);
        this.signIn();
        this.setData({
          userInfo: res.userInfo,
          hasUserInfo: true
        })
      }
    } else {
      // 在没有 open-type=getUserInfo 版本的兼容处理
      wx.getUserInfo({
        success: res => {
          Object.assign(app.globalData, res);
          this.signIn();
          this.setData({
            userInfo: res.userInfo,
            hasUserInfo: true
          })
        }
      })
    } // 注册
  },
  getUserInfo: function (e) {
    console.log(e);
    this.signIn();
    Object.assign(app.globalData, res);
    this.setData({
      userInfo: e.detail.userInfo,
      hasUserInfo: true
    })
  },
  signIn() {
    console.log(app.globalData)

    Api.signIn({
      data: {
        // wxUserInfoDTO: {
        "code": app.globalData.code,
        "encryptedData": app.globalData.encryptedData,
        "iv": app.globalData.iv,
        "rawData": app.globalData.rawData,
        "signature": app.globalData.signature
        // }
      },
      success: (res) => {
        console.log(res.data)
        if (res.code) {
          wx.showToast({
            title: res.data.message,
            icon: 'fail',
            duration: 2000
          })
        }
        // pagethis.setData({
        //   response: res.data
        // })
      },
      fail: (res) => {
        console.log('++++++++++++错误信息++++++++++++++')
        console.log(res)
      }
    })
  },
  changeIndicatorDots(e) {
    this.setData({
      indicatorDots: !this.data.indicatorDots
    })
  },
  changeAutoplay(e) {
    this.setData({
      autoplay: !this.data.autoplay
    })
  },
  intervalChange(e) {
    this.setData({
      interval: e.detail.value
    })
  },
  durationChange(e) {
    this.setData({
      duration: e.detail.value
    })
  },
  // 底部导航切换
  handleChange({ detail }) {
    console.log(detail)
    this.setData({
      current: detail.key
    });
    switch (detail.key) {
      case 'homepage':
        break;
      case 'brush':
        this.handleBrushOpen();
        break;
      default:
        break;
    }
  },
  // 打开发布类型选择
  handleBrushOpen() {
    this.setData({
      brushTypeShow: true
    });
  },
  // 处理发布类型点击
  handleBrushClick({ detail }) {
    console.log(detail)
    // 关闭弹框
    this.setData({
      brushTypeShow: false
    });
    wx.navigateTo({
      url: '../brush/brush?type=' + (detail.index+1)
    })
  },
  getStartPlace() {
    console.log('startPlace');
    app.getLocation('startPlace');
  },
  getEndPlace() {
    app.getLocation('endPlace');
  },
  setAddressInfo(name, data) {
    console.log(name,data)
    this.setData({
      [name]: data
    })
  },
})
