//app.js
import WxValidate from 'utils/WxValidate'

App({
  onLaunch: function () {
    // 展示本地存储能力
    var logs = wx.getStorageSync('logs') || []
    logs.unshift(Date.now())
    wx.setStorageSync('logs', logs)

    // 登录
    wx.login({
      success: res => {
        // 发送 res.code 到后台换取 openId, sessionKey, unionId
        console.log(res);
        this.globalData.code = res.code;
      }
    })
    // 获取用户信息
    wx.getSetting({
      success: res => {
        if (res.authSetting['scope.userInfo']) {
          // 已经授权，可以直接调用 getUserInfo 获取头像昵称，不会弹框
          wx.getUserInfo({
            success: res => {
              Object.assign(this.globalData,res);
              console.log(res)
              
              // 由于 getUserInfo 是网络请求，可能会在 Page.onLoad 之后才返回
              // 所以此处加入 callback 以防止这种情况
              if (this.userInfoReadyCallback) {
                this.userInfoReadyCallback(res);
              }
            }
          })
        }
      }
    })
  }, 
  userInfoReadyCallback(res){
    Object.assign(this.globalData, res)
  },
  getLocation(target){
    wx.navigateTo({
      url: '/pages/address/address?name=' + target
    });
  },
  WxValidate: (rules, messages) => new WxValidate(rules, messages),
  globalData: {
    userInfo: null,
    code: '',
    encryptedData: '',
    iv: ''
  }
})