// pages/address/address.js
// 引入SDK核心类 
const QQMapWX = require('../../qqmap-wx-jssdk.min.js');

// 实例化API核心类
const demo = new QQMapWX({
  key: 'NLXBZ-TNJW6-2DOSB-EJHNA-Q7VWZ-JUBVO' // 必填
});

Page({
  /**
   * 页面的初始数据
   */
  data: {
    // 地图搜索结果
    mapSearchLists:[],
    targetObjName:'',
    // 吸顶位置记录
    scrollTop: 0
  },
  onLoad (options) {
    if (options.name) {
      const targetObjName = options.name || '';
      this.setData({
        targetObjName: targetObjName
      })
    }
  },
  onChange:function({detail}){
    console.log(detail);
    const that = this;
    if (detail === '') {
      that.setData({
        mapSearchLists: []
      })
      return false;
    }
    demo.getSuggestion({
          keyword: detail,
          success: function (res) {
                console.log(res);
                if(res.status === 0 && res.data.length > 1) {
                  that.setData({
                    mapSearchLists: res.data
                  })
                }
          },
          fail: function (res) {
                console.log(res);
          },
          complete: function (res) {
                console.log(res);
          }
    });
  },
  onCancel:function(){

  },
  selectAddress:function(event){
    const that = this;
    const index = Number(event.target.id);
    const pages = getCurrentPages();
    console.log(pages);
    if (pages.length > 1) {
      //上一个页面实例对象
      var prePage = pages[pages.length - 2];
      //关键在这里,设置前一个页面的数据
      console.log(that.data.targetObjName);
      prePage.setData({
        [that.data.targetObjName]: that.data.mapSearchLists[index]
      })
      // prePage.setAddressInfo(that.targetObjName,that.data.mapSearchLists[index]);
      wx.navigateBack(1);
    }

  },
  //页面滚动执行方式
  onPageScroll(event) {
    this.setData({
      scrollTop: event.scrollTop
    })
  }
})